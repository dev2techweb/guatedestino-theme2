<?php get_header(); ?>
<div class="pagina_curso">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 curso">
        <div class="titulo">
          <img src="<?php the_field('imagen'); ?>" alt="">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="descripcion card">
          <div class="card-header">
            Descripción
          </div>
          <div class="card-body">
            <!-- <h5 class="card-title">Des</h5> -->
            <p class="card-text" id="texto-vermas"><?php the_field('descripcion'); ?></p>
          </div>
        </div>
        <div class="modulos accordion" id="accordionExample">
          <div class="titulo">
            <h1>Programa</h1>
          </div>
          <?php 
          $modulos_br = get_field('lecciones');
          $modulos = explode(";", $modulos_br);
          $i=0;
          foreach($modulos as $modulo){
            if($modulo!=""){
              echo "<div class=\"modulo card\">";
              $modulo_el = explode("#", $modulo);
              echo "<div class=\"card-header\" id=\"heading".$i."\"><h2 class=\"mb-0\"><button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapse".$i."\"aria-expanded=\"true\" aria-controls=\"collapse".$i."\">
                  ".$modulo_el[0]."</button></h2></div>";
              echo "<div class=\"lecciones\">";
              if($i==0){
                echo "<div id=\"collapse".$i."\" class=\"collapse\" aria-labelledby=\"heading".$i."\" data-parent=\"#accordionExample\">
                <div class=\"card-body\">";
              }
              else{
                echo "<div id=\"collapse".$i."\" class=\"collapse\" aria-labelledby=\"heading".$i."\" data-parent=\"#accordionExample\">
                <div class=\"card-body\">";
              }
              $lecciones = explode(",",$modulo_el[1]);
              echo "<ul class=\"list-group\">";
              foreach($lecciones as $leccion){
                echo "<li class=\"list-group-item\">";
                echo "<div class=\"leccion\">".$leccion."</div>";
                echo "</li>";
              }
              echo "</ul>";
              echo "</div></div></div>";
              echo "</div>";
            }
            $i++;
          }
        ?>
        </div>
      </div>
      <div class="col-md-4 instructor_precio">
        <div class="precio">
          <h2>Inversión</h2>
          <h3>Q. <?php the_field('precio'); ?></h3>
        </div>
        <div class="instructor card">
          <div class="card-header" style="width:100%;">
            Instructor
          </div>
          <div class="card-body">
            <!-- <h5 class="card-title">Des</h5> -->
            <div class="nombre">
              <img src="<?php $inst = get_field('instructor');	 echo $inst['foto']; ?>" alt="">
              <h4>
                <?php echo $inst['nombre']; ?>
              </h4>
            </div>
            <p class="descripcion">
              <?php echo $inst['descripcion']; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function () {
    var showChar = 300;
    var content = jQuery('#texto-vermas').html();
    if (content.length > showChar) {
      var c = content.substr(0, showChar);
      var html = '<input id="texto-vermas-element-toggle" type="checkbox" /><div id="texto-vermas-abstract">' + c +
        '...' + '</div>' + '<div id="texto-vermas-toggled-element">' + content + '</div>' +
        '<label for="texto-vermas-element-toggle" id="texto-vermas-trigger-toggle"><span class="morelink">Ver más</span><span class="lesslink">Ver Menos</span></label>';
      jQuery('#texto-vermas').html(html);
    }
  });
</script>
<?php get_footer(); ?>