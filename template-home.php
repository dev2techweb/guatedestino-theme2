<?php
/*
* Template Name: Home
*/
get_header(); ?>
<section id="inicio">
  <!-- imagen -->
  <img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.png" alt="">
  <div class="texto">
    <h1>Formando el desarrollo <br> personal y profesional</h1>
    <button data-toggle="modal" data-target="#serviceModal2" servicio="Ingles">
      RESERVA YA
    </button>
  </div>
</section>
<section id="quienessomos">
  <h1>Quienes somos</h1>
  <p>
  En CDI somos una empresa con responsabilidad social. Especialistas  en servicios de capacitaciones In company/abierto, utilizando las metodologías mas avanzadas de formación integral.
  </p>
  <div class="misionyvision container-fluid">
    <div class="row">
      <div class="col-sm-4 elemento">
        <h3>Misión</h3>
        <p>
        Somos una empresa de formación ejecutiva, nos encargamos de formar el desarrollo personal y profesional de nuestros clientes, consolidándonos como personas de valor en su entorno social y empresarial a través de los programas de capacitación. 
        </p>
      </div>
      <div class="col-sm-4 elemento">
        <h3>Visión</h3>
        <p>
        Posicionarnos como empresa solida en el desarrollo de las capacidades del talento humano, de forma empresarial y personal  a nivel regional generando innovación.
        </p>
      </div>
    </div>
  </div>
</section>
<section id="servicios">
  <h1>Nuestros servicios</h1>
  <div class="card-columns">
    <div class="card">
      <div class="card-body">
        <div class="enlace">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/capacitaciones.png" alt="">
          <div class="titulo">
            capacitaciones
            <a class="boton-shadow" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Capacitaciones' ) ) ); ?>">
              ver más
            </a>
          </div>
        </div>
        <p>
        La capacitación se define como el conjunto de actividades didácticas, orientadas a ampliar los conocimientos, habilidades y aptitudes del personal que labora en una organización. La capacitación les permite a los colaboradores tener un mejor desempeño en sus actuales y futuros cargos, adaptándose a las exigencias cambiantes del entorno. <br>
La capacitación tiene una duración mínima de 2 horas y 4 horas máximo.</p>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="enlace">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/diplomado.png" alt="">
          <div class="titulo">
            diplomados
            <a class="boton-shadow" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Diplomados' ) ) ); ?>">
              ver más
            </a>
          </div>
        </div>
        <p>
        Son cursos de estudio que tienen como objetivo profundizar y/o actualizar en temas específicos del área de conocimiento. Son estructurados en módulos sobre temas determinados. <br>
Un diplomado tiene una duración mínima de 6 meses y 12 meses máximo.</p>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="enlace">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/seminario.png" alt="">
          <div class="titulo">
            Seminarios
            <a class="boton-shadow" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Seminarios' ) ) ); ?>">
              ver más
            </a>
          </div>
        </div>
        <p>
        Es una reunión especializada, de naturaleza técnica o académica, que realiza un estudio profundo sobre una determinada materia, el aprendizaje de un seminario es activo ya que el participante debe tener interacción. <br>
Por lo general se establece que el seminario debe tener una duración mínima de 8 horas y máxima de 16 horas.</p>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="enlace">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/workshop.png" alt="">
          <div class="titulo">
            Workshops
            <a class="boton-shadow" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Workshops' ) ) ); ?>">
              ver más
            </a>
          </div>
        </div>
        <p>
        Un Workshop o Taller tiene la finalidad de preparar y desarrollar las aptitudes de forma intensiva a sus participantes con un conjunto de actividades didácticas, orientadas a ampliar los conocimientos, habilidades y aptitudes del personal que labora en una organización así como intercambio de ideas con las que se pretende crear análisis en cada uno sobre diferentes casos. Estos talleres les permiten a los colaboradores tener un mejor desempeño en sus actuales y futuros cargos, adaptándose a las exigencias cambiantes del entorno. <br>
Un Workshop tiene una duración de 4 hasta 8 horas.</p>
      </div>
    </div>
  </div>
</section>
<section id="metodologia">
  <h1>
    Metodología
  </h1>
  <p>
  Nuestra metodología se caracteriza por ser dinámica y que el aprendizaje obtenido pueda ser transmitido a las situaciones laborales y a los puestos de trabajo donde se desempeñan los participantes, utilizando herramientas como talleres, simulaciones y dinámicas de grupo.
  </p>
</section>
<section id="calendario">
  <h1>Calendario</h1>
  <div class="cont">
    <?php echo do_shortcode(get_field('shortcode')); ?>
  </div>
</section>
<section id="areas">
  <h1>Implementamos capacitaciones en <br>todas las áreas profesionales</h1>
  <div class="card-columns">
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/finanzas.png" alt="">
          finanzas
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operaciones.png" alt="">
          operaciones
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/recursos.png" alt="">
          rrhh
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mercadeo.png" alt="">
          mercadeo
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gerencia general.png" alt="">
          gerencia general
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/planeacion estrategica.png" alt="">
          planeación <br> estratégica
        </div>
      </div>
    </div>
    
</section>

<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel"
  aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="serviceModal2" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel"
  aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer()?>