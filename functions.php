<?php
/**
* Theme functions and definitions
*@link https://developer.wordpress.org/themes/basics/theme-functions/
*
*@package WordPress
*@subpackage tech
*@since 1.0.0
*@version 1.0.0
*/

// Scripts & Styles
function smart_styles () {
  wp_register_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.1.1' );
  wp_register_style('icons', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0');
  wp_register_style('icons', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0');
  wp_register_style('style', get_template_directory_uri() . '/style.css', array('bootstrap'), '1.9.1');
  wp_register_style('aos', get_template_directory_uri() . '/assets/aos.css', array('bootstrap'), '1.0.0');
  wp_register_style('tech', get_template_directory_uri() . '/assets/css/tech.css', array('style'), '1.2.10');
  wp_register_style('carousel3D', get_template_directory_uri() . '/assets/css/carousel3D.css', array('style'), '1.0.2');
  wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400', array('tech'), '1.0.0');
  wp_enqueue_style('bootstrap');
  wp_enqueue_style('icons');
  wp_enqueue_style('style');
  wp_enqueue_style('aos');
  wp_enqueue_style('tech');
  wp_enqueue_style('carousel3D');
  wp_enqueue_style('fonts');
  wp_enqueue_script('jquery');
  wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '4.0.0', true );
  wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.2', true);
  wp_enqueue_script('c3d', get_template_directory_uri() . '/assets/js/carousel3D.js', array('jquery'), '1.0.2', true);
}
add_action('wp_enqueue_scripts', 'smart_styles');


/* Paginas */
//create the homepage on activation
if (isset($_GET['activated']) && is_admin()){
  $new_page_title = 'Inicio'; //page title
  $new_page_content = ''; //add here the content of the page
  $new_page_template = 'template-home.php';
  $page_check = get_page_by_title($new_page_title);
  $new_page = array(
    'post_type' => 'page',
    'post_title' => $new_page_title,
    'post_content' => $new_page_content,
    'post_status' => 'publish',
    'post_author' => 1,
    'post_slug'     => 'inicio'
  );
  if(!isset($page_check->ID)){
    $new_page_id = wp_insert_post($new_page);
    if(!empty($new_page_template)){
      update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
    }
  }
}

//menu
function mis_menus() {
  register_nav_menus(
    array(
      'principal' => __( 'Menu principal' ),
    )
  );
}
add_action( 'init', 'mis_menus' );

//crear clase para <a>
add_filter( 'nav_menu_link_attributes', 'clase_menu', 10, 3 );

function clase_menu($atts, $item, $args){
  $class = 'nav-link';
  $atts['class'] = $class;
  return $atts;
}



// post thumbnails

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
}