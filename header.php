<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php wp_title('-',true,'right'); ?><?php bloginfo('name');?></title>
  <?php wp_head(); ?>
</head>
<body>
  <header class="header">
    <div class="menu">
      <nav class="navbar-expand-lg header__nav">
        <button class="navbar-toggler e-center" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="fa fa-bars t-black"></span>
        </button>
          <?php wp_nav_menu(array(
            'theme_location' => 'principal',
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'navbarNav',
            'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
            //clase del elemento li
            'menu_class' => 'nav-item'
          )); ?>
      </nav>
      <?php echo do_shortcode('[responsive_menu]'); ?>
    </div>
    <div class="social">
          <a href="">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/idioma.png" alt="">
          </a>
    </div>
    <div class="logo">
      <a href="<?php echo esc_url(home_url('/'));?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
      </a>
    </div>
    
  </header>
  <main class="main">
 
