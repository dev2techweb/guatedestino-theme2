<?php
/*
* Template Name: Calendario
*/
get_header(); ?>
<div class="calendario">
  <div class="title">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tercera/proximas excursiones.png" alt="">
    <div class="texto">
      Próximas <br> Excursiones
    </div>
  </div>
  <div class="calendar">
  <?php echo do_shortcode('[MEC id="11"]'); ?>
  </div>
</div>

<?php get_footer()?>
