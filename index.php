<?php get_header(); ?>

<section id="inicio">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item full-height active">
      <div class="full-height d-flex a-center j-center" id="s1">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner/1.png" alt="">
      </div>
    </div>
    <div class="carousel-item full-height">
      <div class="full-height d-flex a-center j-center" id="s2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner/2.png" alt="">
      </div>
    </div>
    <div class="carousel-item full-height">
      <div class="full-height d-flex a-center j-center" id="s3">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner/3.png" alt="">
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</section>
<script src="<your-url-here>/assets/modules/channel-web/inject.js"></script>
<script>
  window.botpressWebChat.init({ host: '<your-url-here>', botId: '<your-bot-id>' })
</script>
<section id="nosotros">
  <div class="title">
    <img class="fondo" src="<?php echo get_template_directory_uri(); ?>/assets/img/figura de banner.png" alt="">
    <div class="texto">
      ¡CONOCE MÁS DE <br> &nbsp;&nbsp;&nbsp;&nbsp; <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icono 1.png" alt=""> NOSOTROS!
    </div>
  </div>
  <div class="lista container-fluid">
    <div class="row">
      <div class="col-md-4 elemento" data-aos="fade-up" data-aos-delay="400">
        <div class="title-elemento">
          misión
        </div>
        <p>
        Ser una operadora turística que cubra y supere las expectativas de nuestros clientes, brindando los servicios ofrecidos a través de nuestros diferentes canales, a manera de seguir promoviendo la actividad turística con responsabilidad social y sostenibilidad en todos nuestros destinos.
        </p>
      </div>
      <div class="col-md-4 elemento" data-aos="fade-up" data-aos-delay="200">
        <div class="title-elemento">
          visión
        </div>
        <p>
        Consolidarnos como líderes en el sector de Turismo Nacional, ofreciendo programas originales con tarifas competitivas, mostrando diversidad y un gran abanico de actividades que tenemos para ofrecer a todo tipo de viajero, siendo un referente, ofreciendo calidad y garantía, basándonos en nuestros valores y en nuestro capital humano. 
        </p>
      </div>
      <div class="col-md-4 elemento" data-aos="fade-up" data-aos-delay="400">
        <div class="title-elemento">
          Valores
        </div>
        <p>
        Como GuateDestino nos esforzamos día con día por brindar el mejor servicio a todos nuestros clientes, tanto nacionales como extranjeros, por lo que nuestra empresa se edifica en los siguientes valores y principios: 
          <ul>
            <li>Honestidad</li>
            <li>Amabilidad</li>
            <li>Confiabilidad</li>
            <li>Puntualidad</li>
            <li>Trabajo en Equipo</li>
            <li>Pasión y Compromiso</li>
          </ul>
        </p>
      </div>
    </div>
  </div>



</section>
<div class="seccion1">
<?php echo do_shortcode('[oxilab_flip_box id="1"]'); ?>
<?php echo do_shortcode('[oxilab_flip_box id="2"]'); ?>
<?php echo do_shortcode('[oxilab_flip_box id="3"]'); ?>
</div>

<div class="seccionres">
  <?php echo do_shortcode('[oxilab_flip_box id="4"]'); ?>
  <?php echo do_shortcode('[oxilab_flip_box id="5"]'); ?>
  <?php echo do_shortcode('[oxilab_flip_box id="6"]'); ?>
  <?php echo do_shortcode('[oxilab_flip_box id="7"]'); ?>
</div>

<section id="form_contacto">
  <div class="title">
    Contacto
  </div>
  <div class="form">
  <?php echo do_shortcode('[contact-form-7 id="49" title="Formulario de contacto 1"]'); ?>
  </div>
</section>
<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel"
  aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="serviceModal2" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel"
  aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer()?>