<?php
/*
* Template Name: Contacto
*/
get_header(); ?>

<section id="inicio" class="pagina_contacto">
  <div class="col1" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/banner-con-recuadro.png')">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/reloj.png" alt="">
    <h1 class="t-white">CONTACTO</h1>
  </div>
  <div class="col2">
  <div class="container-fluid">
    <div class="formulario_contacto">
          <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; endif; ?>
        </div>
    </div>
  </div>
</section>
<section id="contacto">
  <div class="col1">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
  </div>
  <div class="col2">
    <a href="tel:+50255555555">
      <h3>Teléfono:<br>5555 5555</h3>
    </a>
    <br>
    <a href="mailto:info@watchesgt.com">
      <h3>Correo:<br>info@watchesgt.com</h3>
    </a>
  </div>
</section>
<?php get_footer()?>











<!-- 

<div class="container-fluid">
  <div class="row a-center" style="background-color: #e50051;background-size:cover; background-position: center;align-items:flex-start;">
    <div class="full-width" style="position:absolute;background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/foto contacto.png');background-size:cover;background-repeat: no-repeat;background-position:top;min-height:115%;">

    </div>
    <div class="col-lg-12 t-center a-center j-center d-flex contacto" style="padding:5rem; padding-top:2rem; flex-direction:column;">
      <div class="formulario_contacto">
        <h2 class="t-pink t-bold">Contacto</h2>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer()?> -->
