<?php
/*
* Template Name: Alianzas
*/
get_header(); ?>
<div class="alianzas">
  <div class="title">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/nuestras alianzas.png" alt="">
    <div class="texto">
      ¡Nuestras <br> Alianzas!
    </div>
  </div>
  <p class="descripcion">
  Desde la creación de GuateDestino en septiembre de 2018, hemos ido creando e innovando servicios al alcance de todos los clientes, nacionales y extranjeros, esto con el fin de brindar un servicio único y sobre todo, personalizado. Entre los servicios que ofrecemos están:
  </p>
  <div class="lista">
    <div class="grupo">
      <a class="btn" onclick="ShowModal('Transportes','')">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/transporte.png" alt="">
        <h4>Transporte</h4>
      </a>
      <a class="btn" onclick="ShowModal('Hoteles','')">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/hoteles.png" alt="">
        <h4>Variedad de <br>Hoteles</h4>
      </a>
    </div>
    <div class="grupo">
    <a class="btn" onclick="ShowModal('Asistencia perzonalizada','')">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/asistencia.png" alt="">
        <h4>Asistencia <br>personalizada</h4>
      </a>
      <a class="btn" onclick="ShowModal('Viajes todos los días','')">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/viajes.png" alt="">
        <h4>Viajes todos <br>los días</h4>
      </a>
    </div>
    <a class="btn" onclick="ShowModal('Promociones','')">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/segunda/promociones.png" alt="">
      <h4>Promociones</h4>
    </a>
  </div>
</div>

<?php get_footer()?>
