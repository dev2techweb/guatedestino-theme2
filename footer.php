<section id="contacto" class="container-fluid">
<div class="lista">
      <div class="info">
          Agencia de viajes <br>
          Operadora de turismo interno y receptivo <br>
          Resolución no. 265-2019-DG <br>
          De fecha 07 de marzo de 2019
      </div>
      <div class="social">
          <a href="https://www.facebook.com/guatedestino/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png" alt="">
          </a>
          <a href="https://www.instagram.com/guatedestino/?hl=es-la">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig.png" alt="">
          </a>
          <a href="https://wa.me/50231245453">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsapp.png" alt="">
          </a>
      </div>
      <div class="grupo">
          <div class="tel">
            <strong>1500</strong> <br>
            <small>ASISTENCIA AL TURISMO</small> <br>
            24212810
          </div>
          <div class="inguat">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo de inguat.png" alt="">
          </div>
      </div>
    </div>
</section>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="container">
            </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function ShowModal(header, content) {
        $('#myModalLabel').html(header);
        if(header=="Sacatepequez"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Sacatepequez.png" style="width: 100%;" alt="">');
        }
        else if(header=="Sololá"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Sololá.png" style="width: 100%;" alt="">');
        }
        else if(header=="Petén"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Petén.png" style="width: 100%;" alt="">');
        }
        else if(header=="Guatemala"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Guatemala.png" style="width: 100%;" alt="">');
        }
        else if(header=="Retalhuleu"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Retahuleu.png" style="width: 100%;" alt="">');
        }
        else if(header=="Izabal"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Izabal.png" style="width: 100%;" alt="">');
        }
        else if(header=="Santa Rosa"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/Santa-Rosa.png" style="width: 100%;" alt="">');
        }
        else if(header=="Transportes"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/transportes-1.png" style="width: 100%;" alt="">');
        }
        else if(header=="Viajes todos los días"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/VIAJES-TODOS-LOS-DÌAS.png" style="width: 100%;" alt="">');
        }
        else if(header=="Promociones"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/PROMOCIONES-Y-CANJES.png" style="width: 100%;" alt="">');
        }
        else if(header=="Hoteles"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/hoteles.png" style="width: 100%;" alt="">');
        }
        else if(header=="Asistencia perzonalizada"){
          $('#container').html('<img src="https://guatedestino.com/wp-content/uploads/2019/09/asesoria-personalizada.png" style="width: 100%;" alt="">');
        }
        else{
          $('#container').html(content);
        // $('#container').removeAttr('class'); //if alredy exists remove it
        // $('#container').addClass(contentClassID);
        }
        $('#myModal').modal('show');
    }
</script>




</main>
<script src="<?php echo get_template_directory_uri(); ?>/assets/aos.js"></script>
  <script>
AOS.init(
  {
    once: true
  }
);
</script>
  <footer class="footer item__padding">
    <span class="rights__text">Este sitio fue creado por <a href="https://techwebgt.com" class="text-white">TechWeb</a> - © <?php echo date('Y'); ?></span>
    <!-- <nav class="social__nav col d-flex j-center">
      <ul class="nav">
        <li class="nav-item">
          <a href="https://www.instagram.com/smartservicegt/" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-instagram"></i></a>
        </li>
        <li class="nav-item">
          <a href="https://www.facebook.com/SmartService-1554850278122848/" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-facebook-square"></i></a>
        </li>
        <li class="nav-item">
          <a href="https://www.youtube.com/channel/UCZxpgWuqPKHi6dVeyAenPwQ" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-youtube"></i></a>
        </li>
      </ul>
    </nav> -->
  </footer>
  <?php wp_footer(); ?>
</body>
</html>
