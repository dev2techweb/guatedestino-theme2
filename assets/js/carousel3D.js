var carousel = document.querySelector('.carousel3D .carousel'); //contenedor del carousel
var cells = carousel.querySelectorAll('.carousel3D .carousel__cell'); //elementos del carousel
var cellCount; // cellCount set from cells-range input value
var selectedIndex = 0;
var cellWidth = carousel.offsetWidth;
var cellHeight = carousel.offsetHeight;
var isHorizontal = true;
var rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
var radius, theta;
var fondo = 1;
// console.log( cellWidth, cellHeight );

function rotateCarousel() {
  if(fondo==1){
    $('#destinos').addClass("fondo1");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo6");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==2){
    $('#destinos').addClass("fondo2");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo6");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==3){
    $('#destinos').addClass("fondo3");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo6");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==4){
    $('#destinos').addClass("fondo4");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo6");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==5){
    $('#destinos').addClass("fondo5");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo6");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==6){
    $('#destinos').addClass("fondo6");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo7");
  }
  else if(fondo==7){
    $('#destinos').addClass("fondo7");
    $('#destinos').removeClass("fondo2");
    $('#destinos').removeClass("fondo3");
    $('#destinos').removeClass("fondo1");
    $('#destinos').removeClass("fondo5");
    $('#destinos').removeClass("fondo4");
    $('#destinos').removeClass("fondo6");
  }
  var angle = theta * selectedIndex * -1;
  carousel.style.transform = 'translateZ(' + -radius + 'px) ' + 
    rotateFn + '(' + angle + 'deg)';
}

var prevButton = document.querySelector('.carousel3D .previous-button');
prevButton.addEventListener( 'click', function() {
  selectedIndex--;
  rotateCarousel();
  fondo--;
  if(fondo==0){
    fondo=7;
  }
});

var nextButton = document.querySelector('.carousel3D .next-button');
nextButton.addEventListener( 'click', function() {
  fondo++;
  if(fondo==8){
    fondo=1;
  }
  selectedIndex++;
  rotateCarousel();
});

// var cellsRange = document.querySelector('.carousel3D .cells-range');
// cellsRange.addEventListener( 'change', changeCarousel );
// cellsRange.addEventListener( 'input', changeCarousel );



function changeCarousel() {
//   cellCount = cellsRange.value;
  cellCount = 7
  theta = 360 / cellCount;
  var cellSize = isHorizontal ? cellWidth : cellHeight;
  radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
  for ( var i=0; i < cells.length; i++ ) {
    var cell = cells[i];
    if ( i < cellCount ) {
      // visible cell
      cell.style.opacity = 1;
      var cellAngle = theta * i;
      cell.style.transform = rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
    } else {
      // hidden cell
      cell.style.opacity = 0;
      cell.style.transform = 'none';
    }
  }

  rotateCarousel();
}

// var orientationRadios = document.querySelectorAll('input[name="orientation"]');
// ( function() {
//   for ( var i=0; i < orientationRadios.length; i++ ) {
//     var radio = orientationRadios[i];
//     radio.addEventListener( 'change', onOrientationChange );
//   }
// })();

function onOrientationChange() {
  //var checkedRadio = document.querySelector('input[name="orientation"]:checked');
  // isHorizontal = checkedRadio.value == 'horizontal';
  isHorizontal = true;
  rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
  changeCarousel();
}

// set initials
onOrientationChange();
